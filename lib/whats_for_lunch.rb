require 'json'
require 'open-uri'
require 'unirest'

class WhatsForLunch
    attr_accessor :available_ingredients
    
    def initialize(available_ingredients=[])
        self.available_ingredients = available_ingredients
    end

    def console_commands(command)
        commands = command.split(' ')
        if commands[0] == "help"
            return help_doc(commands[1])
        elsif commands[0] == "add"
            return add_ingredient(commands[1])
        elsif commands[0] == "search"
            return search(commands[1] ||= "10")
        elsif commands[0] == "display"
            return display_results(commands[1])
        elsif commands[0] == "pull"
            return pull_recipe(commands[1])
        elsif commands[0] == "cls"
            return reset_screen
        else print "Please enter a valid command."
        end
    end


    def help_doc(param)
        reset_screen
        if param == nil
            "You can type 'help' followed with the command you want help with.\nExample: 'help display'"
        elsif param == "add"
            print "You can type add followed by an ingredient.\nExample: add lettuce"
        elsif param == "search"
            print "The search command will search the database and store the results."
        elsif param == "display"
            print "The display command will show the amount of results you specify.\nExample: display 5"
        elsif param == "pull"
            print "The pull command will pull the ingredients list for the selected recipe.\nExample: pull 5"
        else print "Please enter a valid command."
        end
    end
    
    def add_ingredient(ingredient)
        available_ingredients << ingredient
        print "#{ingredient} added to the list"
    end

#    def search_link
#        dev_key = JSON.parse(File.read('data/food2fork.json')).values[0]
#        ingredients_link = available_ingredients.join(',')
#        raw_link = "http://food2fork.com/api/search?key=#{dev_key}&q=#{ingredients_link}"
#        link = URI::encode(raw_link)
#        File.write('search_results.json', open(link).read)
#        reset_screen
#    end
    
    def search(n)
        dev_key = JSON.parse(File.read('data/spoonacular.json')).values[0]
        ingredients_link = available_ingredients.join(' ')
        response = Unirest.get "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/findByIngredients?fillIngredients=false&ingredients=#{ingredients_link}&limitLicense=false&number=#{n}&ranking=1",
        headers:{
            "X-Mashape-Key" => dev_key,
            "Accept" => "application/json"
        }
        File.write('response.json', response.raw_body)
        display_results(n)
    end
    
    def display_results(n)
        string = ""
        recipes_array = JSON.parse(File.read('response.json'))
        n.to_i.times do |i|
            if recipes_array[i] != nil
                string += "#{(i + 1)}) " + recipes_array[i]["title"]
                break if recipes_array[i + 1] == nil
                string += "\n" if i < (n.to_i - 1)
            end
        end
        print "\n"
        print string.gsub(/&amp;/, "&") + "\n"
    end
    
#    def pull_recipe(recipe)
#        recipe_data = JSON.parse(File.read('results.json'))[recipe.to_i - 1]
#        recipe_id = recipe_data["recipe_id"]
#        recipe_name = recipe_data["title"]
#        dev_key = JSON.parse(File.read('data/food2fork.json')).values[0]
#        raw_link = "http://food2fork.com/api/get?key=#{dev_key}&rId=#{recipe_id}"
#        if ! File.exist? "recipes/#{recipe_id}.json"
#            link = URI::encode(raw_link)
#            File.write("recipes/#{recipe_id}.json", open(link).read)
#        end
#        reset_screen
#        print "\n####{recipe_name}###\n\n"
#        ingredients_list = JSON.parse(File.read("recipes/#{recipe_id}.json")).values[0]["ingredients"]
#        puts ingredients_list
#    end
    
        def pull_recipe(recipe)
        recipe_data = JSON.parse(File.read('response.json'))[recipe.to_i - 1]
        recipe_id = recipe_data["id"]
        recipe_name = recipe_data["title"]
        dev_key = JSON.parse(File.read('data/spoonacular.json')).values[0]
        if ! File.exist? "recipes/spoonacular/#{recipe_id}.json"
            response = Unirest.get "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/#{recipe_id}/information?includeNutrition=false",
            headers:{
               "X-Mashape-Key" => "#{dev_key}"
            }
            File.write("recipes/spoonacular/#{recipe_id}.json", response.raw_body)
        end
        reset_screen
        print "\n####{recipe_name}###\n\n"
        ingredients_list = []
        ingredients_array = JSON.parse(File.read("recipes/spoonacular/#{recipe_id}.json"))["extendedIngredients"]
        ingredients_array.each do |i|
            ingredients_list << i["originalString"]
        end
        puts ingredients_list
    end

end