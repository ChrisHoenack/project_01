require_relative 'whats_for_lunch'

wfl = WhatsForLunch.new()

def reset_screen
    system "clear" or system "cls"
    puts " _    _ _           _       ______         _                      _     \n| |  | | |         | |      |  ___|       | |                    | |    \n| |  | | |__   __ _| |_ ___ | |_ ___  _ __| |    _   _ _ __   ___| |__  \n| |/\\| | '_ \\ / _` | __/ __||  _/ _ \\| '__| |   | | | | '_ \\ / __| '_ \\ \n\\  /\\  / | | | (_| | |_\\__ \\| || (_) | |  | |___| |_| | | | | (__| | | |\n \\/  \\/|_| |_|\\__,_|\\__|___/\\_| \\___/|_|  \\_____/\\__,_|_| |_|\\___|_| |_|"
    puts "Available commands are 'help', 'add', 'search', 'display' and 'pull'.\nType 'quit' to exit.\nPlease enter a command:"
end

reset_screen

while true
    print '>>> '
    input = gets.chomp.downcase
    break if input == 'quit'
    puts wfl.console_commands(input)
end

