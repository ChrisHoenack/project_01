require 'json'
require 'whats_for_lunch'

describe WhatsForLunch do
    
    let(:wfltest) { WhatsForLunch.new() }
    
    describe '#console_commands' do
        it 'calls help_doc' do
            expect(wfltest).to receive(:help_doc).and_return(nil)
            wfltest.console_commands('help')
        end
        it 'calls add_ingredient and passes ingredient' do
            expect(wfltest).to receive(:add_ingredient).with('salt').and_return(nil)
            wfltest.console_commands('add salt')
        end
        it 'calls search' do
            expect(wfltest).to receive(:search).and_return(nil)
            wfltest.console_commands('search')
        end
        it 'calls display_results and passes a criteria' do
            expect(wfltest).to receive(:display_results).with('5').and_return(nil)
            wfltest.console_commands('display 5')
        end
        it 'calls pull_recipe and passes a criteria' do
            expect(wfltest).to receive(:pull_recipe).with('1').and_return(nil)
            wfltest.console_commands('pull 1')
        end
        it 'calls reset_screen' do
            expect(wfltest).to receive(:reset_screen).and_return(nil)
            wfltest.console_commands('cls')
        end
    end
    
    describe '#add_ingredient' do
        it 'adds ingredients to list' do
            wfltest.console_commands('add salt')
            wfltest.console_commands('add lettuce')
            expect(wfltest.available_ingredients).to eq(["salt","lettuce"])
        end
    end
    
    describe '#display_results' do
        it "displays 'n' results" do
            expect { wfltest.display_results("5") }.to output("").to_stdout
        end
    end
end